FROM alpine:3.15
RUN    apk update \
    && apk add clamav-scanner clamav-libunrar \
    && sed '/NotifyClamd.*/d' -i /etc/clamav/freshclam.conf \
    && echo 'Updating virus databases using freshclam. This may take few seconds ...' \
    && freshclam

ENTRYPOINT ["/entrypoint.sh"]

COPY entrypoint.sh /entrypoint.sh

# The entrypoint.sh script has mechanism to update it's database on each container run.
# This may seem un-necessary, but from my point of view it is necessary.
# It is a good idea to run the scan with latest virus signatures.
#
# Also, the entrypoint runs a non-destructive scan on '/scanme' directory.
# So, if you mount a host directory on '/scanme' then this container will automatically scan it at boot time.
# Check the entpoint.sh script for more details.



# There is no real daemon/service in this image, 
#   so I simply chose to make the image sleep for one second, and then let it die.
# Having a CMD is important, because some CI systems like Gitlab 
#   expects a CMD to function correctly. 
# If I remove this, then Gitlab goes crazy, 
#   and does not run clamscan command I specfied in .gitlab-ci.yml .
#   
CMD ["sleep" , "1s"]

# Notes:
# ------
# * freshclam expects to notify clamav daemon after the virus database update is complete, 
#     by updating /etc/clamav/clamd.conf file.
#     Since there is no clamav daemon in this image, the file does not exist, 
#     and thus the notification attempt fails. 
#     This is handled by deleting the NotifyClamd directive in the freshclam config file.
#
# clamscan exit codes:
# -------------------
# 0 : No virus found.
# 1 : Virus(es) found.
# 2 : Some error(s) occured.

