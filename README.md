# ClamAV Virus Scanner
This image helps scan files and directories for virus infection. Great to use it in your CI/CD pipelines! Great to use it as a standalone container to scan any directory on your local-computer.

## Features:
* Based on Alpine Linux 
* ClamAV Scanner CLI - to scan files and directories
* Freshclam - to update virus databases
* Auto virus database update on each container run through `/entrypoint.sh`
* No daemon/service, and no need to run the image in daemon/detached mode
* Automatically scans a host directory, if mounted on `/scanme` mountpoint in the container
* Safe/non-destructive scan - i.e. does not repair/disinfect/delete/move/quarantine any files
* Delivers a summarized report of infected files only

**Note:** Does not include `clamd`, as it is completely unnecessary to have it in the image.

Docker Hub repository URL: [https://hub.docker.com/r/kamranazeem/clamav-virus-scanner](https://hub.docker.com/r/kamranazeem/clamav-virus-scanner)


# Usage:
## Mount a host directory on `/scanme`:
Mount a host directory on `/scanme` mountpoint, and the container will automatically scan it for you.
```
[kamran@kworkhorse ~]$ docker run  \
  -v /home/kamran/Projects/Personal/gitlab/:/scanme  \
  kamranazeem/clamav-virus-scanner

Running ENTRYPOINT script: /entrypoint.sh ...

Running 'freshclam' to update virus database(s) ... This may take few seconds ...

ClamAV update process started at Sat Apr 18 23:15:21 2020
DON'T PANIC! Read https://www.clamav.net/documents/upgrading-clamav
daily.cvd database is up to date (version: 25786, sigs: 2270866, f-level: 63, builder: raynman)
WARNING: Your ClamAV installation is OUTDATED!
WARNING: Local version: 0.102.1 Recommended version: 0.102.2
main.cvd database is up to date (version: 59, sigs: 4564902, f-level: 60, builder: sigmgr)
bytecode.cvd database is up to date (version: 331, sigs: 94, f-level: 63, builder: anvilleg)

Found /scanme . Proceeding to scan /scanme for virus infected files ...
Relax! This is a non-destructive scan. It does not repair/disinfect/delete/move/quarantine any files :)

Executing clamscan  --infected  --suppress-ok-results --recursive  /scanme


----------- SCAN SUMMARY -----------
Known viruses: 6825256
Engine version: 0.102.1
Scanned directories: 677
Scanned files: 905
Infected files: 0
Data scanned: 0.88 MB
Data read: 1.11 MB (ratio 0.79:1)
Time: 22.071 sec (0 m 22 s)

Now EXEC-uting CMD sleep 1s ...

[kamran@kworkhorse ~]$  
```

If you don't mount anything on `/scanme`, then the image does not do anything.
```
[kamran@kworkhorse ~]$ docker run  kamranazeem/clamav-virus-scanner

Running ENTRYPOINT script: /entrypoint.sh ...

Running 'freshclam' to update virus database(s) ... This may take few seconds ...

ClamAV update process started at Sat Apr 18 21:45:37 2020
WARNING: Your ClamAV installation is OUTDATED!
WARNING: Local version: 0.102.1 Recommended version: 0.102.2
DON'T PANIC! Read https://www.clamav.net/documents/upgrading-clamav
daily.cvd database is up to date (version: 25786, sigs: 2270866, f-level: 63, builder: raynman)
main.cvd database is up to date (version: 59, sigs: 4564902, f-level: 60, builder: sigmgr)
bytecode.cvd database is up to date (version: 331, sigs: 94, f-level: 63, builder: anvilleg)

/scanme not found. Skipping scan on /scanme ...

[kamran@kworkhorse ~]$ 
```

## Skipping the `/entrypoint.sh` script:
If you don't want to run the virus update process on container start-up, or you don't want your mounted directory to be scanned automatically, then pass `--entrypoint=""` to the `docker run` command, and run it interactively using `-it` and use `/bin/sh` as shell interpreter.

```
[kamran@kworkhorse ~]$ docker run --rm  -it \
  -v /home/kamran/Projects/Personal/gitlab/:/scanme  \
  --entrypoint="" \
  kamranazeem/clamav-virus-scanner \
  /bin/sh
/ #
```

Once you are inside the container, on the shell terminal, feel free to operate the `/scanme` directory.

```
/ # ls /scanme/
clamav-virus-scanner      learn-php                 simple-php                simpleapp.demo.wbitt.com  staticwebsite
geany-setup               learn-php-salman-sb       simple-php-website        ssh-keys
gitlab-ci-demo            php-crud                  simple-website            ssh-server
/ # 

/ # clamscan --recursive /scanme/
. . . 
. . . 
/scanme/staticwebsite/README.md: OK
/scanme/staticwebsite/.gitlab-ci.yml: OK
/scanme/staticwebsite/deployment.yaml.template: OK
/scanme/staticwebsite/Dockerfile: OK

----------- SCAN SUMMARY -----------
Known viruses: 6825256
Engine version: 0.102.1
Scanned directories: 656
Scanned files: 880
Infected files: 0
Data scanned: 0.87 MB
Data read: 1.11 MB (ratio 0.79:1)
Time: 22.835 sec (0 m 22 s)
/ # 
```

## Use it in your CI/CD pipeline - Gitlab:
To check your code for virus infected files, add a job in your `.gitlab-ci.yml` file. An example is shown below:

```
[kamran@kworkhorse gitlab-ci-demo]$ cat .gitlab-ci.yml 

stages:
  - test

# run-lint is a 'job' , which belongs to the test 'stage'.
run-lint:
  image: php:7
  stage: test
  script:
    - pwd
    - find . -type f -name '*.php' -exec php -l {} \;
  only:
    - master

# virus-check is a 'job' , which belongs to the test 'stage'.
virus-check:
  image: kamranazeem/clamav-virus-scanner
  stage: test
  script:
    - clamscan  --infected  --suppress-ok-results --recursive  .
  only:
    - master
[kamran@kworkhorse gitlab-ci-demo]$ 
```

Here is how it looks like if no virus infected files are found by the CI server:

| ![images/gitlab-ci-no-virus-found.png](images/gitlab-ci-no-virus-found.png) |
| --------------------------------------------------------------------------- |

Here is how it looks like if virus infected files *are* found by the CI server:

| ![images/gitlab-ci-virus-found.png](images/gitlab-ci-virus-found.png) |
| --------------------------------------------------------------------- |



Happy scanning!
