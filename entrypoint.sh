#!/bin/sh
# Run freshclam at container start time to update virus database(s)
echo
echo "Running ENTRYPOINT script: /entrypoint.sh ..."
echo
echo "Running 'freshclam' to update virus database(s) ... This may take few seconds ..."
echo
freshclam
echo

# Note:
# '/scanme' does not exist in the file-system of this image.
# However, if the user mounts a directory on /scanme, 
#   then the '/scanme' directory will exist (becomes available), 
#   and the clamscan command can be run on that.
# Otherwise, the script continues and does whatever is configured in CMD in Dockerfile,
#   or passed on the docker run command. 

SCAN_DIR=/scanme
if [ -d ${SCAN_DIR} ] ; then
  echo "Found ${SCAN_DIR} . Proceeding to scan ${SCAN_DIR} for virus infected files ..."
  echo "Relax! This is a non-destructive scan. It does not repair/disinfect/delete/move/quarantine any files :)"
  echo
  echo "Executing: clamscan  --infected  --suppress-ok-results --recursive  ${SCAN_DIR}"
  echo
  clamscan  --infected  --suppress-ok-results --recursive  ${SCAN_DIR}
else
  echo "${SCAN_DIR} not found. Skipping scan on ${SCAN_DIR} ..."
fi

echo
# Run the CMD command mentioned in the Dockerfile
echo "Now EXEC-uting: CMD $@ ..."
exec "$@"

# clamscan exit codes:
# 0 : No virus found.
# 1 : Virus(es) found.
# 2 : Some error(s) occured.
